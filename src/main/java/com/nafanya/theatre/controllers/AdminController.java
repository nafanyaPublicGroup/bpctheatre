package com.nafanya.theatre.controllers;

import com.nafanya.theatre.entites.Theatre;
import com.nafanya.theatre.entites.User;
import com.nafanya.theatre.enums.Role;
import com.nafanya.theatre.enums.Status;
import com.nafanya.theatre.repositories.TheatreRepository;
import com.nafanya.theatre.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TheatreRepository theatreRepository;

    @GetMapping("")
    public String homePage(
            @AuthenticationPrincipal UserDetails userDetails,
            Model model
            ) throws UsernameNotFoundException{
        User admin = userRepository.findByLogin(userDetails.getUsername());
        if(admin == null)
            throw new UsernameNotFoundException("Crap :(");
        model.addAttribute("role", admin.getRole());
        model.addAttribute("login", admin.getLogin());
        model.addAttribute("name", admin.getName());
        return "userHome";
    }



    @GetMapping("/applications")
    public String getApplications(Model model){
        model.addAttribute("applicationList", (List<User>)userRepository.findAllByStatus(Status.CHECKING));
        return "applicationList";
    }

    @PostMapping("/applications")
    public String getApplications(
            @RequestParam Long managerId,
            @RequestParam boolean accept,
            Model model
    ){
        Optional<User> optUser = userRepository.findById(managerId);
        if(optUser.isEmpty()) throw new UsernameNotFoundException("Crap :(");
        User user = optUser.get();
        if(!accept) {
            user.setRole(Role.USER);
            user.getTheatre().getManagers().remove(user);
            user.setTheatre(null);
        }
        user.setStatus(Status.ACTIVE);
        userRepository.save(user);
        return getApplications(model);
    }
}
