package com.nafanya.theatre.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nafanya.theatre.entites.User;
import com.nafanya.theatre.enums.Role;
import com.nafanya.theatre.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/auth/")
public class AuthorizationController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(
            @RequestParam(value = "login") String login,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "birthday") String birthday,
            Model model) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        User user = new User(
                login,
                password,
                name,
                format.parse(birthday));
        if (userRepository.existsByLogin(user.getLogin())) {
            model.addAttribute("message", "user exists");
            return "registration";
        }
        user.setRole(Role.USER);
        userRepository.save(user);
        return "redirect:/theatres";
    }

    @GetMapping("/login")
    public String login(){
        return "login";
    }
}
