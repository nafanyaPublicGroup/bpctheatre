package com.nafanya.theatre.controllers;

import com.nafanya.theatre.entites.Theatre;
import com.nafanya.theatre.entites.User;
import com.nafanya.theatre.enums.Role;
import com.nafanya.theatre.enums.Status;
import com.nafanya.theatre.repositories.TheatreRepository;
import com.nafanya.theatre.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(value = "/manager")
public class ManagerController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TheatreRepository theatreRepository;

    @GetMapping("")
    public String managerHome(
            @AuthenticationPrincipal UserDetails userDetails,
            Model model
    ){
        User manager = userRepository.findByLogin(userDetails.getUsername());
        if(manager == null)
            throw new UsernameNotFoundException("Crap :(");
        if(!manager.getRole().equals(Role.MANAGER)) return "redirect:/user";
        model.addAttribute("theatre", manager.getTheatre());
        model.addAttribute("role", manager.getRole());
        model.addAttribute("login", manager.getLogin());
        model.addAttribute("name", manager.getName());
        return "userHome";
    }

    @PostMapping("/application")
    public String application(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam Long theatreId
    ) throws UsernameNotFoundException, ChangeSetPersister.NotFoundException {
        User user = userRepository.findByLogin(userDetails.getUsername());
        if(user == null)
            throw new UsernameNotFoundException("Crap :(");
        user.setStatus(Status.CHECKING);
        user.setRole(Role.MANAGER);
        Optional<Theatre> optTheatre = theatreRepository.findById(theatreId);
        if(optTheatre.isEmpty()) throw new ChangeSetPersister.NotFoundException();
        Theatre theatre = optTheatre.get();
        theatre.getManagers().add(user);
        user.setTheatre(theatre);
        theatreRepository.save(theatre);
        userRepository.save(user);
        return "redirect:/auth/login";
    }

    @GetMapping("/actors")
    public String getActors(
            @AuthenticationPrincipal UserDetails userDetails,
            Model model
    ) {
        User manager = userRepository.findByLogin(userDetails.getUsername());
        if (manager == null) throw new UsernameNotFoundException("Crap :(");
        model.addAttribute("actors", manager.getTheatre().getActors());
        return "actorList";
    }

    @DeleteMapping("/actors")
    public String deleteActor(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam Long actorId,
            Model model
    ){
        User manager = userRepository.findByLogin(userDetails.getUsername());
        Optional<User> optActor = userRepository.findById(actorId);
        if(manager == null || optActor.isEmpty()) throw new UsernameNotFoundException("Crap :(");
        if(manager.getTheatre() != optActor.get().getTheatre()) throw new IllegalCallerException("Crap :(");
        User actor = optActor.get();
        actor.getTheatre().getActors().remove(actor);
        actor.setTheatre(null);
        actor.setRole(Role.USER);
        userRepository.save(actor);
        return getActors(userDetails, model);
    }
    @PostMapping("/actors")
    public String addActor(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam Long actorId
    ){
        User manager = userRepository.findByLogin(userDetails.getUsername());
        Optional<User> optActor = userRepository.findById(actorId);
        if(manager == null || optActor.isEmpty()) throw new UsernameNotFoundException("Crap :(");
        User actor = optActor.get();
        actor.setRole(Role.ACTOR);
        actor.setTheatre(manager.getTheatre());
        actor.getTheatre().getActors().add(actor);
        userRepository.save(actor);
        userRepository.save(manager);
        return "redirect:/user/list";
    }
}
