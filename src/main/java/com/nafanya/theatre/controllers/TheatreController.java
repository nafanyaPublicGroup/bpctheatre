package com.nafanya.theatre.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nafanya.theatre.entites.Theatre;
import com.nafanya.theatre.repositories.TheatreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
public class TheatreController {

    @Autowired
    private TheatreRepository theatreRepository;

    @GetMapping("/theatres")
    public String get(Model model){
        model.addAttribute("theatres", theatreRepository.findAll());
        return "theatres";
    }

    @GetMapping("/theatres/{id}")
    public String getTheatre(
            @PathVariable(value = "id") Long id,
            Model model
    ) throws ClassNotFoundException {
        Optional<Theatre> optTheatre = theatreRepository.findById(id);
        if(optTheatre.isEmpty()) throw new ClassNotFoundException("Crap :(");
        Theatre theatre = optTheatre.get();
        model.addAttribute("theatre", theatre);
        return "theatre";
    }
    @PostMapping("/theatres")
    public void put (
            @RequestBody String form,
            HttpServletResponse response
    ){

        JsonObject object = new Gson().fromJson(form, JsonObject.class);
        Theatre theatre = new Theatre(
                object.get("name").getAsString(),
                object.get("city").getAsString(),
                object.get("address").getAsString());
        if(!theatreRepository.existsByNameAndCityAndAddress(theatre.getName(), theatre.getCity(), theatre.getAddress()))
            theatreRepository.save(theatre);
        else
            response.setStatus(HttpServletResponse.SC_CONFLICT);
    }
}