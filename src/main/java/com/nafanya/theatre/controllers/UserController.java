package com.nafanya.theatre.controllers;

import com.nafanya.theatre.entites.User;
import com.nafanya.theatre.enums.Status;
import com.nafanya.theatre.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("")
    public String homePage(
            @AuthenticationPrincipal UserDetails currentUser,
            Model model){
        User user = userRepository.findByLogin(currentUser.getUsername());
        if(user == null)
            return "home";
        String returnValue = "";
        switch (user.getRole()){
            case ACTOR:
                model.addAttribute("theatre", user.getTheatre());
            case USER:
                returnValue = "userHome";
                break;
            case ADMIN:
                return "redirect:/admin";
            case MANAGER:
//                model.addAttribute("theatre", user.getTheatre());
                return "redirect:/manager";
        }
        model.addAttribute("role", user.getRole());
        model.addAttribute("login", user.getLogin());
        model.addAttribute("name", user.getName());
        return returnValue;
    }

    @GetMapping("/list")
    public String getAllUsers(
            @AuthenticationPrincipal UserDetails userDetails,
            Model model
    ){ //FOR ADMIN
        User user = userRepository.findByLogin(userDetails.getUsername());
        if(user == null) throw new UsernameNotFoundException("Crap :(");
        model.addAttribute("requester", user);
        model.addAttribute("userList", (List<User>)userRepository.findAllByStatus(Status.ACTIVE));
        return "usersList";
    }
}
