package com.nafanya.theatre.repositories;

import com.nafanya.theatre.entites.User;
import com.nafanya.theatre.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(String login);

    boolean existsByLogin(String login);

    List<User> findAllByStatus(Status status);
}
