package com.nafanya.theatre.repositories;

import com.nafanya.theatre.entites.Theatre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TheatreRepository extends JpaRepository<Theatre, Long> {
    boolean existsByNameAndCityAndAddress(String name, String city, String address);
}