package com.nafanya.theatre.enums;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public enum Role{
    USER(Set.of(
            Permission.THEATRE_GET,
            Permission.ACTORS_GET
    )),

    MANAGER(Set.of(
            Permission.THEATRE_GET,
            Permission.ACTORS_GET,
            Permission.ACTORS_POST,
            Permission.USER_GET
    )),

    ACTOR(USER.permissions),
    ADMIN(Set.of(
            Permission.THEATRE_GET,
            Permission.THEATRE_POST,
            Permission.USER_GET,
            Permission.USER_POST,
            Permission.ACTORS_GET,
            Permission.ACTORS_POST
    ));

    private final Set<Permission> permissions;

    Role(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Permission> getPermissions(){
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        Set<SimpleGrantedAuthority> authorities = getPermissions()
                .stream().map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + name()));
        return authorities;
    }
}