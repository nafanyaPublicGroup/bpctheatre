package com.nafanya.theatre.enums;

public enum Permission {
    THEATRE_GET("theatre:get"),
    THEATRE_POST("theatre:post"),

    ACTORS_GET("actors:get"),
    ACTORS_POST("actors:post"),

    USER_GET("user:get"),
    USER_POST("user:post");
    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission(){
        return permission;
    }
}
