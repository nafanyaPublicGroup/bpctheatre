package com.nafanya.theatre.entites;

import com.nafanya.theatre.enums.Role;

import com.nafanya.theatre.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "usr")

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String login;

    private String password;

    private Date birthday;

    private String name;

    private String avatarPath;

    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private Status status;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    Theatre theatre;

    public User(String login, String password){
        this.login = login;
        this.password = new BCryptPasswordEncoder(12).encode(password);
        status = Status.ACTIVE;
        name = "Джонни Бройлеров";
        avatarPath = "";
        role = Role.USER;
        birthday = new Date();
        birthday.setYear(1990);
        birthday.setMonth(1);
        birthday.setDate(1);
    }

    public User(String login, String password, String name){
        this(login, password);
        this.name = name;
    }

    public User(String login, String password, String name, Date birthday){
        this(login, password, name);
        this.birthday = birthday;
    }
}
