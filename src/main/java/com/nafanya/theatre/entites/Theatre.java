package com.nafanya.theatre.entites;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

//HIBERNATE
@Entity
@Table(name = "theaters")
//LOMBOK
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Theatre implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String city;

    private String address;

    private Integer rating;

    private String description;

    @OneToMany(
//            mappedBy = "users",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE}
    )
    List<User> managers;
    @OneToMany(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE}
    )
    List<User> actors;

    public Theatre(String name, String city, String address){
        this.name = name;
        this.city = city;
        this.address = address;
        rating = 0;
    }

    public Theatre(String name, String city, String address, String description){
        this(name, city, address);
        this.description = description;
    }
}
